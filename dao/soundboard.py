from os import path
from config.settings import soundspath
from discord import Embed
from dao.database import DatabaseDAO

class SoundboardDAO(DatabaseDAO):

    def __init__(self, database, collection):

        super().__init__(database=database,collection=collection)

        self.directory = f"{soundspath}/{collection}"

    def get_random(self):
        """Return the path to a random file"""

        random = self.get_one()
        path = random.get('file')
        return f"{self.directory}/{path}"

    def get_sound_by_name(self,name):
        """
        Return the path of a sound file.

        Params:
            name(str): the string to filter

        Return(str):
            The absolute path to the sound file
        """
        query  = {'title':
            {
                '$regex' : f"{name}",
                '$options' : 'i'
            }
        }

        sound = self.get_one_by_query(query)

        if sound is not None:
            return f"{self.directory}/{sound.get('file')}"
        return None

    def get_sounds_by_name(self,name):
        """
        Return a list of document titles matching a string

        Params:
            name(str): the string to filter

        Return(list): the list of document titles
        """

        query  = {'title':
            {
                '$regex' : f"{name}",
                '$options' : 'i'
            }
        }

        sounds = self.get_many_by_query(query)

        return_list = []

        for elem in sounds:
            return_list.append(elem.get('title'))
        if return_list != []:
            return return_list
        return None

    def to_message(self,result_list:list):
        """
        Return a formated message from a list of paths

        Params:
            result_list(list): list of file paths (string)

        Return(str): the formated message

        """
        message = "```\n"
        for elem in result_list:
            message += f"{elem}\n"
        message += "```"
        return message
