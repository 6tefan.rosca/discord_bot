import re
from random import randint, choice

from pymongo import MongoClient

from config.settings import mongodb as mdb
class DatabaseDAO():

    def __init__(self, database, collection):
        self.client = MongoClient(mdb.get("host"), mdb.get('port'))
        self.database = self.client.get_database(database)
        self.collection = self.database.get_collection(collection)

    def get_one(self):
        """ Return a random element from the collection"""

        one = self.collection.aggregate([
            { "$match": { "start_time": { "$exists": False } } },
            { "$sample": { "size": 1 } }
        ])
        return list(one)[0]

    def get_one_by_query(self, query):
        """
        Return a random element matching a query

        Params:
            query(dict): the json query

        Returns(dict): the random result
        """

        return self.collection.find_one(query)

    def get_many_by_query(self,query):
        """
        Return a list of elements matching a query

        Params:
            query(dict): the json query

        Returns(list): the list of match
        """
        result = self.collection.find(query)
        return list(result)

    def add_one(self, document):
        """
        Add a new document to a collection

        Params:
            document(dict): json object to add

        Return: Bool
        """
        insert = self.collection.insert_one(document)
        return insert.acknowledged

    def add_many(self, document_list):
        """
        Add a list of document to a collection

        Params:
            document_list(list): list of json objects to add

        Return:Bool
        """
        insert = self.collection.insert_many(document_list)
        return insert.acknowledged

    def delete_one(self,document):
        """
        Remove one document from a colletion

        Params:
            document(dict): json object to remove

        Return: Bool
        """

        delete = self.collection.delete_one(document)
        return delete.acknowledged


    def delete_many(self,document_list):
        """
        Remove a list of documents from a colletion

        Params:
            document(list): list of json objects to remove

        Return: Bool
        """

        delete = self.collection.delete_many(document_list)
        return delete.acknowledged


    def update_one(self,document, updated):
        """
        Update one document from a colletion

        Params:
            document(dict): json object to update
            update(dict): the updated json

        Return: Bool
        """

        updated= self.collection.update_one(document, {"$set":updated})
        return updated.acknowledged
