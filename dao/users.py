from datetime import datetime, timedelta
from pymongo import MongoClient
from dao.database import DatabaseDAO
from dto.user import DiscordUser


class UserDAO(DatabaseDAO):

    def __init__(self, database, collection):

        super().__init__(database=database, collection=collection)

    def add_user(self, id, name):
        """
        Add a new user to the collection

        Param:
            uid(int): discord uid of a user,
            guild(int)

        Return: Bool
        """

        user = DiscordUser(id=id, name=name)
        user_tojson = user.to_json()
        return  self.add_one(user.to_json())

    def get_user(self, id):
        """
        Return a user to the collection

        Params:
            id(int): discord uid of a user,

        Return: DiscordUser
        """
        result= self.get_one_by_query({"id":id})
        return DiscordUser(result.get('id'), result.get('name'), result.get('last_connection'))



    def update_user(self, user):
        """
        Update a user in the collection

        Params:
            user(dict): instance of DiscordUser

        Return: result
        """
        last_connection = datetime.now()
        return self.update_one(user.to_json(), {"last_connection":last_connection})

    def delete_user(self, user):
        """
        Remove a user from the collection

        Params:
            user(dict): instance of DiscordUser.to_json

        Return: Bool
        """
        return self.delete_one(user)


    def get_all_by_timestamp(self,delta):

        delta = timedelta(days=delta)
        timestamp = datetime.now() - delta

        query = {
            'last_connection':{
                '$lt':timestamp
            }
        }
        return list(self.get_many_by_query(query))
