# discord_marion

# Deployment

## Python Dependencies
Create a virtualenv, source it and install the dependencies

```bash
python3 -m virtualenv virtualenv
source ./virtualenv/bin/activate
pip3 install -r requirements.txt
```
## Config file
Create a copy of the settings.py.dst located in /config, rename it settings.py and
fill the blanks

```bash
cp ./config/settings.py.dst ./config/settings.py
```

## Third Party Dependencies

To benefit from the voice_connect feature (the purge message after 30 of voice inactivity)
you will need :

- A [mongodb](https://docs.mongodb.com/) deamon running with the default port.

- A [crontab](https://en.wikipedia.org/wiki/Cron) with the following command : `0 20 10 * * python3 ~/path/to/bot/dir/curl.py


## Discord Server Setup

The bot needs access to the manage_messages and voice_channel permissions among other things.

You also need to create a webhook to trigger the purge message function.
