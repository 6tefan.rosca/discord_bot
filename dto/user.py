from datetime import datetime

class DiscordUser():

    def __init__(self, id, name, last_connection=datetime.now()):
        self.id = id
        self.name = name
        self.last_connection = last_connection

    def to_json(self):
        user_json = {
            "id":self.id,
            "name":self.name,
            "last_connection":self.last_connection
        }
        return user_json
