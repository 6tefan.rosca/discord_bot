import argparse
import re
from os import listdir
from os.path import isfile, join

from pymongo import MongoClient

#Parser
parser = argparse.ArgumentParser(description="Process root dir and mongodb params")
parser.add_argument('-f', metavar='sound folder', type=str, help="the absolute path to the sound folder")
parser.add_argument('-d', metavar='database', type=str, help="the name of the mongodb database")
parser.add_argument('-c', metavar='collection', type=str, help="the name of the mongodb collection")

args = parser.parse_args()

#MongoDb
client = MongoClient('localhost', 27017)
discord = client.get_database(args.d)
collection = discord.get_collection(args.c)

def list_dir(sound_folder):
    files= [f for f in listdir(sound_folder) if isfile(join(sound_folder,f))]
    return files

def make_document(sound_file):

    without_extension = re.sub('\.(.){1,3}$', '', sound_file)
    sanitized = re.sub('[^A-Za-z0-9]+', ' ', without_extension)

    jsoned = {
        "file":sound_file,
        "title": sanitized
    }
    return jsoned



def update_db(files):
    for elem in files:
        document = make_document(elem)
        if list(collection.find({"title": document.get("title")})) == []:
            collection.insert_one(document)
            print("inserted", document)
        else:
            print("skip")

update_db(list_dir(args.f))
