from config.settings import bot_channels


def is_voice_connected(ctx):
    try:
        if ctx.author.voice.channel is not None:
            return True
    except:
        return False

def is_botchannel(ctx):
    if ctx.channel.id in bot_channels:
       return True
    return False
