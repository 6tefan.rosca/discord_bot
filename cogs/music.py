import discord
import asyncio

from discord.ext import commands
from services.youtube import YTDLSource
from utils.decorators import is_botchannel, is_voice_connected

class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def join(self, ctx, *, channel: discord.VoiceChannel):
        """Pour rejoindre un channel vocal

        Syntaxe: !join <nom du channel>
        Example: !join Placard
        """
        await ctx.message.delete()
        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)

        await channel.connect()

    @commands.command(pass_context=True)
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def play(self, ctx, *, query):
        """Joue un son stocké sur mon disque dur

        Syntaxe: !play <chemin du fichier>
        Example: !play ./sounds/coco.mp3
        """
        await ctx.message.delete()
        source = discord.PCMVolumeTransformer(
            discord.FFmpegPCMAudio(query), 0.2)
        ctx.voice_client.play(source, after=lambda e: print(
            'Player error: %s' % e) if e else None)

        await ctx.message.delete()

    @commands.command()
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def volume(self, ctx, volume: int):
        """Pour modifier le volume (de 0 à 100)

        Syntaxe: !volume <Pourcentage>
        Example: !volume 30
        """
        await ctx.message.delete()
        if ctx.voice_client is None:
            return await ctx.send("Not connected to a voice channel.")

        ctx.voice_client.source.volume = volume / 100
        await ctx.send("Changed volume to {}%".format(volume))

    @commands.command()
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def pause(self, ctx):
        """Met en pause la lecture en cours

        Syntaxe: !pause
        """
        await ctx.message.delete()
        ctx.voice_client.pause()
        await ctx.message.delete()

    @commands.command()
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def resume(self, ctx):
        """Reprend la lecture en cours"""
        ctx.voice_client.resume()
        await ctx.message.delete()

    @commands.command()
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def stop(self, ctx):
        """Arrête la diffusion en cours et fait sortir le bot du salon vocal"""

        await ctx.voice_client.disconnect()
        await ctx.message.delete()

    @commands.command()
    @commands.check(is_botchannel)
    @commands.check(is_voice_connected)
    async def yt(self, ctx, *, url):
        """
        Joue le son tiré d'une vidéo Youtube

        Syntaxe: !play <url ou recherche youtube
        Example: !play Never gonna give you up
        """

        await ctx.message.delete()
        player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
        ctx.voice_client.play(player, after=lambda e: print(
            'Player error: %s' % e) if e else None)

    @play.before_invoke
    @yt.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError(
                    "Author not connected to a voice channel.")
        elif ctx.voice_client.is_playing():
            ctx.voice_client.stop()
        else:
            userchan = ctx.author.voice.channel
            botchan = ctx.voice_client.channel
            print(userchan,botchan)
            if userchan != botchan:
                await userchan.connect()


    @stop.before_invoke
    async def ensure_novoice(self, ctx):
        if ctx.voice_client is None:
            return



def setup(bot):
    bot.add_cog(Music(bot))
    print("cog music loaded")
