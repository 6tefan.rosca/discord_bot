import discord

from discord.ext import commands

import asyncio

from config.settings import purge_wb, admin_id, guild_id
from dao.users import UserDAO

class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    dbu = UserDAO('discord','users')

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, amount=1):
        """Pour effacer des messages d'un channel (mod onlyx)

        Syntaxe: !clear <quantité ( 1 par défaut)>
        Example: !clear, !clear 5
        """
        await ctx.message.delete()
        await ctx.channel.purge(limit=amount)


    @commands.Cog.listener()
    async def on_message(self, message):
        """
        Handler for the Webhook triggering the recap function
        """
        if message.author.id == purge_wb.get('id'):
            await message.delete()

            admin = self.bot.get_user(admin_id)
            await self.recap(admin)

    @commands.Cog.listener()
    async def on_member_join(member):
        role = get(member.guild.roles, name="Apprentis")
        await member.add_roles(role)


    @commands.Cog.listener()
    async def on_voice_state_update(self,member,before,after):
        """
        Updates the database when someone joins a vocal channel
        """
        user = self.dbu.get_user(member.id)

        if user is None:
            self.dbu.add_user(member.id, member.name)

        else:
            self.dbu.update_user(user)


    async def recap(self, target):
        """
        Sends a recap of all the users AFK for at least 30 days

        Params:
        - target: the user receiving the recap
        """
        embed = discord.Embed()

        timed_out = self.dbu.get_all_by_timestamp(30)

        if timed_out == []:
            return
        for member in timed_out:
            name = member.get('name')
            lc = member.get('last_connection')
            lc = f"{lc.day}/{lc.month}/{lc.year}"
            embed.add_field(name = name, value = lc, inline = False)

        await target.send(content='liste des membres AFK', embed = embed )

def setup(bot):
    print("cog mod loaded")
    bot.add_cog(Moderation(bot))
