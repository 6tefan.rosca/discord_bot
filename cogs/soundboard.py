import asyncio

import discord
from discord import PCMVolumeTransformer, FFmpegPCMAudio
from discord.ext import commands


from dao.soundboard import SoundboardDAO
from utils.decorators import is_botchannel, is_voice_connected

class Soundboard(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.command(pass_context=True)
    @commands.check(is_voice_connected)
    @commands.check(is_botchannel)
    async def kt(self,ctx,*search):
        """Kaamelott Soundboard"""
        await self.play(ctx, 'kaamelott', search)

    @commands.command(pass_context=True)
    @commands.check(is_voice_connected)
    @commands.check(is_botchannel)
    async def meme(self,ctx,*search):
        """Meme Soundboard"""
        await self.play(ctx, 'meme', search)


    async def play(self,ctx,soundboard, search):
        """Generic function for soundboards"""
        await ctx.message.delete()
        try :
            ctx.voice_client.stop()
        except:
            pass

        soundboard = SoundboardDAO("discord",soundboard)
        if len(search) == 0:
            track = soundboard.get_random()

        elif search[0] == 'list' and len(search) > 1:
            search = ' '.join(search[1:])
            tracks = soundboard.get_sounds_by_name(search)
            to_message = soundboard.to_message(tracks)

            await ctx.send(to_message,delete_after=15)
            return

        else:
            search = ' '.join(list(search))
            track = soundboard.get_sound_by_name(search)

        source = FFmpegPCMAudio(track)
        source = PCMVolumeTransformer(source,0.1)

        ctx.voice_client.play(source, after=lambda e: print(
            'Player error: %s' % e) if e else None)


    @kt.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                raise commands.CommandError(
                    "Author not connected to a voice channel.")

        elif ctx.voice_client.is_playing():
            ctx.voice_client.stop()

        elif ctx.voice_client is not None:
            userchan = ctx.author.voice.channel
            botchan = ctx.voice_client.channel
            if userchan != botchan:
                await ctx.voice_client.move_to(userchan)

def setup(bot):
    bot.add_cog(Soundboard(bot))
    print("cog soundboard loaded")
