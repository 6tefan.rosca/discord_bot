import asyncio
import discord
from discord.ext import commands
from discord.utils import find, get

from config.settings import bot_token

bot = commands.Bot(command_prefix="!", description="LOLBOT")

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Game(name="!help"))


cogs = ["cogs.music", "cogs.soundboard", "cogs.moderation"]
# Bot Launcher

if __name__ == "__main__":
    for cog in cogs:
        bot.load_extension(cog)

bot.run(bot_token)
